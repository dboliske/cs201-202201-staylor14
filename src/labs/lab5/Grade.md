# Lab 5

## Total

0/20

## Break Down

CTAStation

- Variables:                    0/2
- Constructors:                 0/1
- Accessors:                    0/2
- Mutators:                     0/2
- toString:                     0/1
- equals:                       0/2

CTAStopApp

- Reads in data:                0/2
- Loops to display menu:        0/2
- Displays station names:       0/1
- Displays stations by access:  0/2
- Displays nearest station:     0/2
- Exits                         0/1

## Comments
Didn't submit