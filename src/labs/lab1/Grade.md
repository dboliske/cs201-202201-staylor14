# Lab 1

## Total

15.6/20

## Break Down

* Exercise 1    2/2
* Exercise 2    2/2
* Exercise 3    2/2
* Exercise 4
  * Program     2/2
  * Test Plan   1/1
* Exercise 5
  * Program     1.5/2
  * Test Plan   1/1
* Exercise 6
  * Program     2/2
  * Test Plan   1/1
* Documentation 5/5

## Comments
- For Ex.5 output should be converted to sq ft
- Late submission (-20%)