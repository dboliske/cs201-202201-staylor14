package labs.lab1;

import java.util.Scanner;

public class InchesConversion {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Input a length in inches:"); // Prompt the user for inches.
		double inches = Double.parseDouble(input.nextLine());
		double centimeters = inches*2.54;              // Convert inches to centimeters
		System.out.println(inches + " inches is " + centimeters + " centimeters"); // Display the result to the console.

		// Test Cases: 
		// inches:2    expected:5.08     actual:5.08     accurate? Yes
		// inches:25   expected:63.5     actual:63.5     accurate? Yes
		// inches:504  expected:1280.16  actual:1280.16  accurate? Yes
		
		
	}

}
