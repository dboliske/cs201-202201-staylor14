package labs.lab1;

import java.util.Scanner;

public class Box {

	public static void main(String[] args) {
		// Prompt the user for the length, width, and depth in inches of a box.
		Scanner input = new Scanner(System.in);
		System.out.println("Enter length in feet: ");
		double length = Double.parseDouble(input.nextLine());
		System.out.println("Enter width in feet: ");
		double width = Double.parseDouble(input.nextLine());
		System.out.println("Enter depth in feet: ");
		double depth = Double.parseDouble(input.nextLine());
		
		// Calculate the amount of wood (square feet) needed to make the box.
		// Display the result to the screen with a descriptive message.
		double surfaceArea = 2*(length*width + length*depth + width*depth);
		System.out.println("You would need "+ surfaceArea + " square feet of wood to make a box with these dimensions.");
		
		// Test Cases:
		// length:3   width:1   depth:2   expected:22    actual:22.0    accurate? Yes
		// length:15  width:8   depth:11  expected:746   actual:746.0   accurate? Yes
		// length:51  width:25  depth:17  expected:5134  actual:5134.0  accurate? Yes
	}

}
