package labs.lab1;

import java.util.Scanner;

public class temperature {

	public static void main(String[] args) {
		// Prompt the user for a temperature in Fahrenheit, convert 
		// the Fahrenheit to Celsius and display the result.
		Scanner input = new Scanner(System.in);
		System.out.print("Input a temperature in Fahrenheit:");
		double degreesF = Double.parseDouble(input.nextLine());
		double degreesC = (degreesF - 32)*5/9;
        System.out.println(degreesF + " degrees fahrenheit is " + degreesC + " degrees celsius.");
        
        // Test Cases:
        // 10 degrees fahrenheit    expected:-12.2222   actual:-12.222222222222221  accurate? Yes
        // 32 degrees fahrenheit    expected:0          actual:0.0                  accurate? Yes
        // 89 degrees fahrenheit    expected:31.6667    actual:31.666666666666668   accurate? Yes
        
        
        
        // Prompt the user for a temperature in Celsius, convert 
        // the Celsius to Fahrenheit and display the result.
		System.out.print("Input a temperature in celsius:");
		double degreesCTwo = Double.parseDouble(input.nextLine());
		double degreesFTwo = degreesCTwo*9/5 + 32;
        System.out.println(degreesCTwo + " degrees celsius is " + degreesFTwo + " degrees fahrenheit.");
        
        // Test Cases: 
        // 5 degrees celsius     expected:41    actual:41.0    accurate? Yes
        // 35 degrees celsius    expected:95    actual:95.0    accurate? Yes
        // 150 degrees celsius   expected:302   actual:302.0   accurate? Yes
        
        
	}

}
