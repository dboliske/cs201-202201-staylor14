package labs.lab1;

import java.util.Scanner;

public class UserInput {

	public static void main(String[] args) {
		// (1) Reading input and writing output: Writing a Java program that will prompt a user for a name;
		// save the input and echo the name to the console.
		
		Scanner input = new Scanner(System.in);    //declare a Scanner object that will read from the keyboard
		System.out.println("Enter a name:");       //prompt user for a name
		String name = input.nextLine();            //save input
		System.out.println(name);                  //output info to the screen (echo)

		// (2) Performing arithmetic calculations: Output the result of the following calculations;
		//be sure to write a descriptive comment for each output so the reader knows what is being calculated:
		
		// Your age subtracted from your father's age
		int dadAgeDiff = 59-19;
		System.out.println(dadAgeDiff);
		
		// Your birth year multiplied by 2
		int birthYearTimesTwo = 2002*2;
		System.out.println(birthYearTimesTwo);
		
		// Convert your height in inches to cms
		double heightInToCm = 68*2.54;
		System.out.println(heightInToCm);
		
		// Convert your height in inches to feet and inches where inches is an integer (mode operator)
		int heightInFt = 68/12;
		int remainingIn = 68%12;
		System.out.println(heightInFt + " feet " + remainingIn + "inches ");
		
		//Prompt a user for a first name; display the user's first initial to the screen.
		System.out.println("Enter a name:");
		char firstInitial = input.nextLine().charAt(0);
		System.out.println(firstInitial);
		
		
		
	}

}
