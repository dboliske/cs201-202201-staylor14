package labs.lab2;

import java.util.Scanner;

public class boxLoop {

	public static void main(String[] args) {
		// program that will prompt the user for a number
		// and print out a square with those dimensions.
		
		Scanner input = new Scanner(System.in);
		System.out.print("Input an integer: ");
		int userValue = Integer.parseInt(input.nextLine());
		
		for (int row=1; row <= userValue; row++) {
			for (int col=1; col <= userValue; col++) {
				System.out.print("* ");
			}
			System.out.println();
			input.close();
		}
		
	}
	}

