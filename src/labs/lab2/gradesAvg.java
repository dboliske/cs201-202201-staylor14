package labs.lab2;

import java.util.Scanner;

public class gradesAvg {

	public static void main(String[] args) {
		// program that will prompt user for grades of an exam, 
		// compute the average, and return the result.
		Scanner input = new Scanner(System.in);
		for(int i = 1; i <= 1; i++) {
		    double sum = 0, count = 0;

		    while(true) {
		        System.out.printf("Enter your grade (input '-1' to exit): ");
		        int grade = Integer.parseInt(input.nextLine());
		        if(grade == -1) break;                  // exit statement
		        sum = sum + grade;
		        count++;
		    }

		    System.out.printf("Your average is: " + sum / count);
		    input.close();
		}

	}

	
}
	

	


