package labs.lab2;

import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {
		// program that will repeatedly display a menu of choices to a user and prompt them to enter an option
		System.out.println("choose an option: ");
		Scanner input = new Scanner(System.in);
		
		boolean done = false; //flag
			do {
				
				System.out.println("1) Say Hello");
				System.out.println("2) Addition");
				System.out.println("3) Multiplication");
				System.out.println("4) Exit");
				System.out.println("Enter your choice: ");
				String choice = input.nextLine();
				
				switch (choice) {
				
					case "1":
						System.out.println("Hello!");
						System.out.println();// for formatting
					break;
					
					case "2"://Addition
						System.out.println("Enter a number: ");
						Double firstNum = Double.parseDouble(input.nextLine());
						System.out.println("Enter another number: ");
						Double secondNum = Double.parseDouble(input.nextLine());
						System.out.println("The sum of the two numbers is " + (firstNum+secondNum));
						System.out.println();
					break;
					
					case "3"://Multiplication
						System.out.println("Enter a number: ");
						Double thirdNum = Double.parseDouble(input.nextLine());
						System.out.println("Enter another number: ");
						Double fourthNum = Double.parseDouble(input.nextLine());
						System.out.println("The product of the two numbers is: " + thirdNum*fourthNum);
						System.out.println();
					break;
					
					case "4": //exit
						done = true;
						System.out.println();
					break;
					
					default://invalid number
						System.out.println("Please enter a valid number.");
						System.out.println();
					break;
					}
				}
				
				while (!done);
				input.close();
				System.out.println("Goodbye!");
					
			}
	}


