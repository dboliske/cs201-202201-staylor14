package labs.lab0;

public class Square {

	public static void main(String[] args) {
		// Pseudo code for making a square:
			// 1) print statement for top of square
			// 2) print statement for sides of square 
		    // 3) repeat to match length of top (*3)
			// 4) print statement for bottom of square
		
		System.out.println("    _________");
		System.out.println("   |         |");
		System.out.println("   |         |");
		System.out.println("   |         |");
		System.out.println("   |_________|");
		
		// result is as expected by following the steps
	}

}
