package exams.first;

import java.util.Scanner;

public class QuestionOne {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.print("Input an integer: ");
		int userValue = Integer.parseInt(input.nextLine());
		input.close();
		
		int addition = userValue + 65;
		int convertToChar = char(addition);
		
		System.out.println(convertToChar);

	}

}
