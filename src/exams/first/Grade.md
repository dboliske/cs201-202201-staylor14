# Midterm Exam

## Total

69/100

## Break Down

1. Data Types:                  18/20
    - Compiles:                 4/5
    - Input:                    5/5
    - Data Types:               4/5
    - Results:                  5/5
2. Selection:                   20/20
    - Compiles:                 5/5
    - Selection Structure:      10/10
    - Results:                  5/5
3. Repetition:                  18/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               3/5
4. Arrays:                      13/20
    - Compiles:                 5/5
    - Array:                    3/5
    - Exit:                     5/5
    - Results:                  0/5
5. Objects:                     0/20
    - Variables:                -/5
    - Constructors:             -/5
    - Accessors and Mutators:   -/5
    - toString and equals:      -/5

## Comments

1. Your type-casting is off causing your program to not run.
2. Good
3. Nearly there, but does a square, not a triangle.
4. Doesn't read in or store strings and doesn't count repeats.
5. Not submitted.
