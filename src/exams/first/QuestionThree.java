package exams.first;

import java.util.Scanner;

public class QuestionThree {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Input an integer: ");
		int userValue = Integer.parseInt(input.nextLine());
		
		for (int row=1; row <= userValue; row++) {
			for (int col=1; col <= userValue; col++) {
				System.out.print("* ");
			}
			System.out.println();
			input.close();

	}

}
}