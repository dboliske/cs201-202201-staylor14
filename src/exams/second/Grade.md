# Final Exam

## Total

0/100

## Break Down

1. Inheritance/Polymorphism:    -/20
    - Superclass:               -/5
    - Subclass:                 -/5
    - Variables:                -/5
    - Methods:                  -/5
2. Abstract Classes:            -/20
    - Superclass:               -/5
    - Subclasses:               -/5
    - Variables:                -/5
    - Methods:                  -/5
3. ArrayLists:                  -/20
    - Compiles:                 -/5
    - ArrayList:                -/5
    - Exits:                    -/5
    - Results:                  -/5
4. Sorting Algorithms:          -/20
    - Compiles:                 -/5
    - Selection Sort:           -/10
    - Results:                  -/5
5. Searching Algorithms:        -/20
    - Compiles:                 -/5
    - Jump Search:              -/10
    - Results:                  -/5

## Comments

1. No submission
2.
3.
4.
5.
